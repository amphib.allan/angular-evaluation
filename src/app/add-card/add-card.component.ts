import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

// import { CartService } from '../cart.service';
import { CardsService } from '../state/cards/cards.service';

@Component({
  selector: 'app-add-card-form',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.css'],
})
export class AddCardFormComponent implements OnInit {
  addCardForm;

  constructor(
    private cardsService: CardsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const currentUserId = parseInt(
      this.route.snapshot.paramMap.get('personId'),
      10,
    );
    this.addCardForm = this.formBuilder.group({
      card_number: '',
      person_id: currentUserId,
    });
  }

  onSubmit(cardInfo) {
    console.warn('You have added a new card', cardInfo);
    this.cardsService.AddCard(cardInfo).subscribe(response => {
      console.log(response);
    });
  }
}
