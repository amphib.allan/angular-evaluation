import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { CardsTableDataSource } from './cards-table.datasource';
import { CardsService } from '../state/cards/cards.service';
import { CreditCards } from '../state/cards/cards.model';

@Component({
  selector: 'app-cards-table',
  templateUrl: './cards-table.component.html',
  styleUrls: ['./cards-table.component.scss'],
})
export class CardsTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<CreditCards>;
  dataSource: CardsTableDataSource;

  constructor(
    private cardsService: CardsService,
    private route: ActivatedRoute,
  ) {}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['card_number', 'balance'];

  ngOnInit() {
    const currentUserId = parseInt(
      this.route.snapshot.paramMap.get('personId'),
      10,
    );
    console.log('currentUserId: ', currentUserId);
    this.dataSource = new CardsTableDataSource();
    this.cardsService.getCards(currentUserId).subscribe(cards => {
      console.log(cards);
      this.dataSource.initData(cards);
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
