export class CreditCards {
  id: number;
  // tslint:disable-next-line: variable-name
  card_number: string;
  // tslint:disable-next-line: variable-name
  person_id: number;
  balance: number;
}
