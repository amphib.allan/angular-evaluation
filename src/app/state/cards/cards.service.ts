import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CreditCards } from './cards.model';

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class CardsService {
  baseUrl = 'http://localhost:3000/card/';

  constructor(private http: HttpClient) {}

  getCards(personId): Observable<CreditCards[]> {
    const userCards = this.http.post<CreditCards[]>(
      this.baseUrl + 'query',
      { person: personId },
      httpOptions,
    );
    return userCards;
  }

  AddCard(cardInfo): Observable<CreditCards[]> {
    return this.http.post<CreditCards[]>(
      this.baseUrl + 'add',
      cardInfo,
      httpOptions,
    );
  }
}
