import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Person } from './people.model';

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json',
  }),
};

@Injectable({ providedIn: 'root' })
export class PeopleService {
  url = 'http://localhost:3000/person/list';
  people: Person[];

  constructor(private http: HttpClient) {}

  getPeople(): Observable<Person[]> {
    const people = this.http.post<Person[]>(`${this.url}`, {});
    return people;
  }
}
