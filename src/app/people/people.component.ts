import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { PeopleDataSource } from './people.datasource';
import { Person } from '../state/people/people.model';
import { PeopleService } from '../state/people/people.service';

@Component({
  selector: 'app-people-table',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss'],
})
export class PeopleComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Person>;
  dataSource: PeopleDataSource;

  constructor(private peopleService: PeopleService) {}

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['first_name', 'last_name', 'email'];

  ngOnInit() {
    this.dataSource = new PeopleDataSource();
    this.peopleService.getPeople().subscribe(people => {
      this.dataSource.initData(people);
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
