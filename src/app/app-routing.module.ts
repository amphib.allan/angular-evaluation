import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleComponent } from './people/people.component';
import { CardsTableComponent } from './cards/cards-table.component';

const routes: Routes = [
  {
    path: '',
    component: PeopleComponent,
  },
  {
    path: 'person/:personId',
    component: CardsTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
